#!/bin/bash

# Function to display ASCII art logo
display_logo() {
    if [ -f /etc/os-release ]; then
        source /etc/os-release
        case $ID in
            "mint")
                echo -e "\e[32m" # green color
                cat << "EOF"
 ___________     
|_          \   
  | | _____ |    
  | | | | | |   
  | | | | | |   
  | \_____/ |    
  \_________/
EOF
                ;;
            "ubuntu")
                echo -e "\e[31m" # Red color
                cat << "EOF"
         _    
     ---(_)   
 _/  ---  \   
(_) |   |     
  \  --- _/    
     ---(_)  

EOF
                ;;
            "arch")
                echo -e "\e[34m" # blue color
                cat << "EOF"
          /\
         /  \
        /    \
       /      \
      /   ,,   \
     /   |  |   \
    /_-''    ''-_\
EOF
                ;;
            "debian")
                echo -e "\e[31m" #red
                cat << "EOF"
  _____     
 /  __ \    
|  /    |    
|  \___-     
-_          
  --_       
       


EOF
                ;;
          "fedora")
                echo -e "\e[34m" # blue
                cat << "EOF"
      _____      
     /   __)\    
     |  /  \ \   
  ___|  |__/ /   
 / (_    _)_/    
/ /  |  |        
\ \__/  |        
 \(_____/ 

EOF
                ;;
                    "opensuse")
                echo -e "\e[32m" # green
                cat << "EOF"
  _______     
__|   __ \     
     / .\ \   
     \__/ |   
   _______|   
   \_______   
__________/ 


EOF
                ;;
                     "zorin")
                echo -e "\e[34m" #blue
                cat << "EOF"
      `osssssssssssssssssssso`           
       .osssssssssssssssssssssso.          
      .+oooooooooooooooooooooooo+.         
                                           
                                           
  `::::::::::::::::::::::.         .:`     
 `+ssssssssssssssssss+:.`     `.:+ssso`    
.ossssssssssssssso/.       `-+ossssssso.   
ssssssssssssso/-`      `-/osssssssssssss    
.ossssssso/-`      .-/ossssssssssssssso.   
 `+sss+:.      `.:+ssssssssssssssssss+`    
  `:.         .::::::::::::::::::::::`     
                                           
                                           
      .+oooooooooooooooooooooooo+.         
       -osssssssssssssssssssssso-          
        `osssssssssssssssssssso`           
                                  



EOF
                ;;

                     "elementary OS")
                echo -e "\e[37m" #white
                cat << "EOF"
         eeeeeeeeeeeeeeeee            
    eeeee  eeeeeeeeeeee   eeeee       
  eeee   eeeee       eee     eeee     
 eeee   eeee          eee     eeee    
eee    eee            eee       eee   
eee   eee            eee        eee   
ee    eee           eeee       eeee   
ee    eee         eeeee      eeeeee   
ee    eee       eeeee      eeeee ee   
eee   eeee   eeeeee      eeeee  eee   
eee    eeeeeeeeee     eeeeee    eee   
 eeeeeeeeeeeeeeeeeeeeeeee    eeeee    
  eeeeeeee eeeeeeeeeeee      eeee     
    eeeee                 eeeee       
      eeeeeee         eeeeeee         
         eeeeeeeeeeeeeeeee            



EOF
                ;;
                 "Manjaro")
                echo -e "\e[32m" #green
                cat << "EOF"
||||||||| ||||   
||||||||| ||||   
||||      ||||   
|||| |||| ||||   
|||| |||| ||||   
|||| |||| ||||   
|||| |||| |||| 


EOF
                ;;


				                 "gentoo")
                echo -e "\e[34m" #blue
                cat << "EOF"
                                                      .
                .vir.                                d$b
              .d$$$$$$b.    .cd$$b.     .d$$b.   d$$$$$$$$$$$b  .d$$b.      .d$$b.
              $$$$( )$$$b d$$$()$$$.   d$$$$$$$b Q$$$$$$$P$$$P.$$$$$$$b.  .$$$$$$$b.
              Q$$$$$$$$$$B$$$$$$$$P"  d$$$PQ$$$$b.   $$$$.   .$$$P' `$$$ .$$$P' `$$$
                "$$$$$$$P Q$$$$$$$b  d$$$P   Q$$$$b  $$$$b   $$$$b..d$$$ $$$$b..d$$$
               d$$$$$$P"   "$$$$$$$$ Q$$$     Q$$$$  $$$$$   `Q$$$$$$$P  `Q$$$$$$$P
              $$$$$$$P       `"""""   ""        ""   Q$$$P     "Q$$$P"     "Q$$$P"
              `Q$$P"                                  """


EOF
                ;;


        esac
    else
        # Default logo
        echo -e "\e[35m" # Magenta color
        cat << "EOF"
          .88888888:.
        88888888.88888.
       .8888888888888888.
       888888888888888888
       88' _`88'_  `88888
       88 88 88 88  88888
       88_88_::_88_:88888
       88:::,::,:::::8888
       88`:::::::::'`8888
      .88  `::::'    8:88.
     .88            .::888.
    .8888. .  ...  .:88888:.
    .8888::.     .::88888::
    .8888'`::. .::'`8888
    .       `::'    `888
               .::::888
              .:::::::8
             .:::::::::
            :::::::::::
     ...       :::::::::::

EOF
    fi
    echo -e "\e[0m" # Reset color
}

# Function to get the package count based on package manager
get_package_count() {
    if command -v dpkg-query &> /dev/null; then
        echo $(dpkg-query -f '${binary:Package}\n' -W | wc -l)
    elif command -v rpm &> /dev/null; then
        echo $(rpm -qa | wc -l)
    elif command -v pacman &> /dev/null; then
        echo $(pacman -Q | wc -l)
    else
        echo "N/A"
    fi
}

# Function to get information about various themes
get_theme_info() {
    gtk_theme=$(gsettings get org.gnome.desktop.interface gtk-theme)
    icon_theme=$(gsettings get org.gnome.desktop.interface icon-theme)
    cursor_theme=$(gsettings get org.gnome.desktop.interface cursor-theme)

    echo -e "\e[33mGTK Theme:\e[0m $gtk_theme"
    echo -e "\e[33mIcon Theme:\e[0m $icon_theme"
    echo -e "\e[33mCursor Theme:\e[0m $cursor_theme"
    
}


# Function to display system information
display_info() {
    os=$(awk -F= '/^PRETTY_NAME=/{print $2}' /etc/os-release | tr -d '"')
    echo -e "\e[33m Operating System:\e[0m $os"
    echo -e "\e[33m  host: \e[0m $(cat /proc/sys/kernel/hostname)"
    echo -e "\e[33m Kernel Name:\e[0m $(uname -s)"
    echo -e "\e[33m Kernel Version:\e[0m $(uname -r)"
    echo -e "\e[33m Kernel Release:\e[0m $(uname -v)"
    echo -e "\e[33m Kernel Architecture:\e[0m $(uname -m)"
    echo -e "\e[33m Uptime:\e[0m $(uptime)"
    echo -e "\e[33m Packages:\e[0m $(get_package_count)"
    echo -e "\e[33m Shell:\e[0m $SHELL"
    echo -e "\e[33m Resolution:\e[0m $(xrandr | grep '*' | awk '{print $1}')"
    echo -e "\e[33m Machine Hardware Platform:\e[0m $(uname -i)"
    echo -e "\e[33m Processor Type:\e[0m $(uname -p)"
    echo -e "\e[33m CPU:\e[0m $(grep "model name" /proc/cpuinfo | cut -d ' ' -f 3- | uniq)"
    echo -e "\e[33m CPU CORES:\e[0m$(awk '/^cpu cores/ {print $4; exit}' /proc/cpuinfo)"
    echo -e "\e[33m CPU THREADS:\e[0m$(awk '/^processor/ {count++} END {print count}' /proc/cpuinfo)"
    echo -e "\e[33m GPU:\e[0m $(lspci | grep VGA | cut -d ':' -f 3 | cut -d '[' -f 1 | sed 's/^ *//')"
    echo -e "\e[33m Memory:\e[0m $(free -h --si | awk '/^Mem/ {print $3 " / " $2}')"
    echo -e "\e[33m Swap:\e[0m $(free -h --si | awk '/^Swap/ {print $3 " / " $2}')"
    echo -e "\e[33m Disk Usage:\e[0m $(df -h / | awk 'NR==2 {print $3 "/" $2 " (" $5 " used)"}')"
    echo -e "\e[33m Second Disk Usage:\e[0m $(df -h /dev/sda1 | awk 'NR==2 {print "(" $2 "/" $3 " used) " $5}')"
    echo -e "\e[33m Other:\e[0m $(df -h /dev/sda | awk 'NR==2 {print "(" $2 "/" $3 " used) " $5}')"
    echo -e "\e[33m Clock/Time:\e[0m $(date '+%Y-%m-%d %H:%M:%S')"
    echo -e "\e[33m BATTERY:\e[0m$(upower -i $(upower -e | grep BAT) | grep --color=never -E "state|to full|percentage")"
    echo -e "\e[33m Mounted Drive/Drives:\e[0m"
     printf "      %-30s %-10s %-10s %-10s\n" "Filesystem" "Size" "Used" "Use%"
  df -h | awk 'NR>1 {printf "      %-30s %-10s %-10s %-10s\n", $1, $2, $3, $5}'
    echo -e ""


}

# Main function to display output
main() {
    display_logo
    display_info
    get_theme_info
}


# Execute main function
main

